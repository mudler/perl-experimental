# Kent Fredric <kentfredric@gmail.com> (24 Jul 2014)
# Virtuals for ex-core perl modules that are now only dev-perl/ depenends.
# Remove any time after August 24.
virtual/perl-Devel-DProf
virtual/perl-Pod-Plainer


# Kent Fredric <kentfredric@gmail.com> (03 Jul 2014)
# Depends on old version of Date-Manip which is no longer available in portage since
# 11 Jun 2014, and upstream are unresponsive.
# Presently unresolvable as lots of things need the newer version, which has a new ABI
# Unmask if upstream fixes Time-Format.
# Trim these modules from tree if this problem exists after Jul 2015
<=dev-perl/Time-Format-0.80.0
<=dev-perl/Weather-Com-2.0.0

# Kent Fredric <kentfredric@gmail.com> (11 Mar 2011)
# Experimental Ebuilds for Perl-Core stuff
perl-core/B-Debug
virtual/perl-B-Debug
perl-core/B-Lint
virtual/perl-B-Lint
perl-core/CPANPLUS-Dist-Build
virtual/perl-CPANPLUS-Dist-Build
perl-core/SelfLoader
virtual/perl-SelfLoader
perl-core/Devel-SelfStubber
virtual/perl-Devel-SelfStubber
perl-core/IPC-SysV
virtual/perl-IPC-SysV
perl-core/NEXT
virtual/perl-NEXT
perl-core/Pod-LaTeX
virtual/perl-Pod-LaTeX
perl-core/Unicode-Collate
virtual/perl-Unicode-Collate
perl-core/Unicode-Normalize
virtual/perl-Unicode-Normalize
